extern crate serde;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate serde_json;

#[macro_use]
extern crate arrayref;

#[macro_use]
extern crate lazy_static;

extern crate hyper;
extern crate rand;
extern crate crypto;
extern crate secstr;
extern crate base64;
extern crate url;
extern crate futures;
extern crate tokio_core;

use futures::*;
use secstr::{SecStr, SecBox};
use std::str::FromStr;
use hyper::Body;
use hyper::client::HttpConnector;

mod errors;
pub use errors::Error;
mod auth;
mod common;

lazy_static! {
    // static ref BASE_URI: url::Url = FromStr::from_str("https://mega.nz").unwrap();
    static ref BASE_API_URL: url::Url = FromStr::from_str("https://g.api.mega.co.nz/cs").unwrap();
}

#[derive(Clone, Debug)]
pub struct Options {}

#[derive(Debug)]
pub struct Client {
    client: hyper::Client<HttpConnector, Body>,
    api_key: String,
    sequence_id: u32,
    session_id: Option<SecStr>,
    master_key: Option<SecStr>,
}

impl Client {
    pub fn new(api_key: &str, client: hyper::Client<HttpConnector, Body>) -> Result<Self, Error> {
        use rand::Rng;
        let mut rng = rand::os::OsRng::new()?;

        Ok(Self {
            client: client,
            api_key: api_key.to_owned(),
            sequence_id: rng.next_u32(),
            session_id: None,
            master_key: None,
        })
    }

    pub fn login<'a>(&'a mut self, email: &str, password: &SecStr) -> Box<Future<Item = (), Error = Error> + 'a> {
        let request_data = auth::LoginRequest::new(email, password);

        Box::new(self.send_request(&request_data).and_then(
            move |value: serde_json::Value| -> Result<(), Error> {
                let response: auth::LoginResponse = serde_json::from_value(value)?;

                self.sequence_id += 1;
                self.master_key = Some(response.decrypted_master_key(request_data.key)?);

                Ok(())
            },
        ))
    }


    fn generate_uri<T: common::Request>(&mut self, request: &T) -> hyper::Uri {
        let mut url = BASE_API_URL.clone();
        {
            let mut builder = url.query_pairs_mut();

            builder.append_pair("id", &self.sequence_id.to_string());
            builder.append_pair("ak", &self.api_key);

            if let Some(ref session_id) = self.session_id {
                let session_id_bytes = session_id.unsecure();
                builder.append_pair("sid", std::str::from_utf8(session_id_bytes).unwrap());
            }

            if let Some(params) = request.additional_query_params() {
                for kv in params {
                    builder.append_pair(kv.0, kv.1);
                }
            }
        }

        url.into_string().parse().unwrap()
    }

    fn send_request<T: common::Request>(&mut self, request: &T) -> Box<Future<Item = serde_json::Value, Error = Error>> {
        use hyper::{Request, header, Method, Chunk};

        let mut req: Request<Body> = Request::new(Method::Post, self.generate_uri(request));

        // setup the request data
        {
            let data = json! {[request]};
            let json = data.to_string();
            let len = json.len() as u64;
            req.set_body(json);
            req.headers_mut().set(header::ContentType::json());
            req.headers_mut().set(header::ContentLength(len));
        }
        Box::new(
            self.client
                .request(req)
                .from_err::<Error>()
                .and_then(
                    move |response: hyper::Response| -> BoxFuture<Chunk, Error> {
                        let status = response.status();
                        if status.is_success() {
                            response.body().concat2().from_err::<Error>().boxed()
                        } else {
                            future::err::<Chunk, _>(Error::FailureStatusCode(status)).boxed()
                        }
                    },
                )
                .and_then(move |body: Chunk| -> Result<serde_json::Value, Error> {
                    use serde_json::Value;
                    let value: Value = serde_json::from_slice(&body)?;

                    if value == Value::Null {
                        Err(Error::EmptyResponse)
                    } else if let Value::Number(n) = value {
                        Err(n.into())
                    } else if let Value::Array(ref arr) = value {
                        // the actual response is inside array, because requests can be batched
                        let len = arr.len();
                        if len == 1 {
                            let item: Value = arr[0].to_owned();
                            if let Value::Number(n) = item {
                                Err(n.into())
                            } else {
                                Ok(item)
                            }
                        } else if len == 0 {
                            Err(Error::EmptyResponse)
                        } else {
                            Err(Error::UnexpectedResponseCount(len))
                        }
                    } else {
                        Err(value.into())
                    }
                }),
        )

    }
}
