use std::collections::HashMap;
use serde::{Serialize, Deserialize};

pub(crate) trait Request: Serialize {
    fn action(&self) -> RequestAction;
    fn additional_query_params(&self) -> Option<&HashMap<&str, &str>>;
}

pub(crate) trait Response<'de>: Deserialize<'de> {
    fn code(&self) -> i32;
}

#[derive(Serialize, Copy, Clone, Debug)]
pub(crate) enum RequestAction {
    #[serde(rename = "us")]
    Login,
}
