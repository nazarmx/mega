use super::*;
use std::collections::HashMap;
use crypto::symmetriccipher::{BlockEncryptor, BlockDecryptor};
use common::*;

const KEY_LEN: usize = 16;
/// AES-128 encryption key (derived from password)
type Key = [u8; KEY_LEN];

const USER_HANDLE_LEN: usize = 11;
/// [User handles](https://docs.mega.nz/sdk/doc/internals.html#user-handles) consist of eleven base-64 characters.
type UserHandle = [u8; USER_HANDLE_LEN];

#[derive(Clone, Debug)]
pub(crate) struct SessionToken {
    pub session_id: SecStr,
    pub master_key: SecStr,
}

lazy_static! {
    static ref HAS_AESNI: bool = crypto::util::supports_aesni();
}

fn new_block_encryptor(key: &Key) -> Box<BlockEncryptor> {
    use crypto::aes::KeySize;
    use crypto::aessafe::AesSafe128Encryptor;
    use crypto::aesni::AesNiEncryptor;

    if *HAS_AESNI {
        Box::new(AesNiEncryptor::new(KeySize::KeySize128, key))
    } else {
        Box::new(AesSafe128Encryptor::new(key))
    }
}

fn new_block_decryptor(key: &Key) -> Box<BlockDecryptor> {
    use crypto::aes::KeySize;
    use crypto::aessafe::AesSafe128Decryptor;
    use crypto::aesni::AesNiDecryptor;

    if *HAS_AESNI {
        Box::new(AesNiDecryptor::new(KeySize::KeySize128, key))
    } else {
        Box::new(AesSafe128Decryptor::new(key))
    }
}

#[derive(Serialize, Clone, Debug)]
pub(crate) struct LoginRequest {
    #[serde(rename = "a")]
    pub action: RequestAction,
    #[serde(rename = "user")]
    pub email: String,
    #[serde(skip)]
    pub key: SecBox<Key>,
    #[serde(rename = "uh")]
    pub user_handle: UserHandle,
}

impl Request for LoginRequest {
    fn action(&self) -> RequestAction {
        self.action
    }
    fn additional_query_params(&self) -> Option<&HashMap<&str, &str>> {
        None
    }
}

impl LoginRequest {
    pub fn new(email: &str, password: &SecStr) -> Self {
        let key = Self::prepare_key(password);
        let email_lowercase = email.to_lowercase();
        let handle = Self::generate_user_handle(&email_lowercase, &key);

        Self {
            action: RequestAction::Login,
            key: key,
            email: email.to_owned(),
            user_handle: handle,
        }
    }

    fn prepare_key(password: &SecStr) -> SecBox<Key> {
        let mut result: Key = [
            0x93,
            0xC4,
            0x67,
            0xE3,
            0x7D,
            0xB0,
            0xC7,
            0xA4,
            0xD1,
            0xBE,
            0x3F,
            0x81,
            0x01,
            0x52,
            0xCB,
            0x56,
        ];

        let pwd_bytes: &[u8] = password.unsecure();

        // 65536 = 2^16 + 1 iterations
        for _ in 0..std::u16::MAX as usize + 1 {
            for chunk in pwd_bytes.chunks(KEY_LEN) {
                let encryptor = if chunk.len() == KEY_LEN {
                    new_block_encryptor(array_ref!(chunk, 0, KEY_LEN))
                } else {
                    let mut temp: Key = [0; KEY_LEN]; // temporary buffer to store current key/block for encryption
                    temp[..chunk.len()].copy_from_slice(chunk);
                    new_block_encryptor(&temp)
                };

                let copy = result;
                encryptor.encrypt_block(&copy, &mut result);
            }
        }
        let boxed_key: Box<Key> = Box::new(result);
        SecBox::new(boxed_key)
    }

    fn generate_user_handle(email_lowercase: &str, key: &SecBox<Key>) -> UserHandle {
        let mut hash: Key = [0; KEY_LEN];

        {
            let data = email_lowercase.as_bytes();
            for (i, x) in data.iter().enumerate() {
                hash[i % KEY_LEN] ^= *x;
            }
        }

        {
            let encryptor = {
                let key_data: &[u8] = key.unsecure();
                assert_eq!(key_data.len(), KEY_LEN);
                let key_ref: &Key = array_ref!(key_data, 0, KEY_LEN);
                new_block_encryptor(key_ref)
            };
            // 16384 = 2^14 iterations
            for _ in 0..16384 {
                let copy = hash;
                encryptor.encrypt_block(&copy, &mut hash);
            }
        }

        let mut hash_parts = [0u8; 8];
        // Retrieve bytes 0-4 and 8-12 from the hash
        hash_parts[0..4].copy_from_slice(&hash[0..4]);
        hash_parts[4..8].copy_from_slice(&hash[8..12]);

        let result: String = base64::encode_config(&hash_parts, base64::URL_SAFE_NO_PAD);
        let mut handle: UserHandle = [0; USER_HANDLE_LEN];
        assert_eq!(result.len(), USER_HANDLE_LEN);
        handle.copy_from_slice(result.as_bytes());

        handle
    }
}

#[derive(Deserialize, Clone, Debug)]
pub(crate) struct LoginResponse {
    #[serde(rename = "csid")]
    pub session_id: SecStr,
    #[serde(rename = "tsid")]
    pub temporary_session_id: SecStr,
    #[serde(rename = "privk")]
    pub private_key: SecStr,
    #[serde(rename = "k")]
    /// Base64 (URL-safe) encoded (encrypted master key)
    pub master_key: SecStr,
}

impl LoginResponse {
    pub fn decrypted_master_key(&self, key: SecBox<Key>) -> Result<SecStr, Error> {
        let master_key_base64: &[u8] = self.master_key.unsecure();
        let mut buf: Vec<u8> = base64::decode_config(master_key_base64, base64::URL_SAFE_NO_PAD)?;

        let decryptor = {
            let key_data: &Key = key.unsecure();
            new_block_decryptor(key_data)
        };

        for mut chunk in buf.chunks_mut(KEY_LEN) {
            assert_eq!(chunk.len(), KEY_LEN);
            let copy: Vec<u8> = chunk.to_owned();
            decryptor.decrypt_block(&copy, &mut chunk);
        }

        Ok(SecStr::new(buf))
    }

    pub fn decrypted_session_id(&self, key: &SecStr) -> SecStr {
        unimplemented!()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn prepare_key() {
        let password = SecStr::from("Hello, world!".repeat(5));
        let key = LoginRequest::prepare_key(&password);
        assert_eq!(
            key.unsecure(),
            &[
                0x1c,
                0x27,
                0xbf,
                0x9a,
                0x44,
                0x79,
                0x20,
                0xb1,
                0x90,
                0xfa,
                0xb0,
                0xa5,
                0x92,
                0x85,
                0x6d,
                0x23
            ]
        );
    }

    #[test]
    fn new_login_request() {
        const EMAIL: &'static str = "email@example.com";
        let password = SecStr::from("Hello, world!".repeat(5));

        let request = LoginRequest::new(EMAIL, &password);
        assert_eq!(request.email, EMAIL);
        assert_eq!(&request.user_handle, b"gnnfZdlIL3k");
    }
}
