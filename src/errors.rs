use super::*;

#[derive(Debug)]
pub enum Error {
    Http(hyper::Error),
    FailureStatusCode(hyper::StatusCode),
    InvalidJson(serde_json::Error),
    ApiFailure(i64),
    InvalidStatusCode(serde_json::Number),
    NonArrayResponse(serde_json::Value),
    EmptyResponse,
    IoError(std::io::Error),
    UnexpectedResponseCount(usize),
    InvalidBase64(base64::DecodeError),
}

impl From<base64::DecodeError> for Error {
    fn from(e: base64::DecodeError) -> Self {
        Error::InvalidBase64(e)
    }
}

impl From<std::io::Error> for Error {
    fn from(e: std::io::Error) -> Self {
        Error::IoError(e)
    }
}

impl From<hyper::Error> for Error {
    fn from(e: hyper::Error) -> Self {
        Error::Http(e)
    }
}

impl From<serde_json::Value> for Error {
    fn from(v: serde_json::Value) -> Self {
        Error::NonArrayResponse(v)
    }
}

impl From<hyper::StatusCode> for Error {
    fn from(e: hyper::StatusCode) -> Self {
        Error::FailureStatusCode(e)
    }
}

impl From<serde_json::Error> for Error {
    fn from(e: serde_json::Error) -> Self {
        Error::InvalidJson(e)
    }
}

impl From<serde_json::Number> for Error {
    fn from(n: serde_json::Number) -> Self {
        if let Some(code) = n.as_i64() {
            Error::ApiFailure(code)
        } else {
            Error::InvalidStatusCode(n)
        }
    }
}
