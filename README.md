# MEGA Rust SDK (Unofficial) [![build status](https://gitlab.com/nazarmx/mega/badges/master/build.svg)](https://gitlab.com/nazarmx/mega/commits/master) [![crates.io](https://img.shields.io/crates/v/mega.svg)](https://img.shields.io/crates/v/mega.svg) [![docs.rs](https://docs.rs/mega/badge.svg)](https://docs.rs/mega/badge.svg)  [![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

## WIP (totally unfinished)

Made with:

- [`hyper`](https://hyper.rs)
- [`tokio`](https://tokio.rs)
- [`rust-crypto`](https://crates.io/crates/rust-crypto)
- [`serde`](https://serde.rs)
